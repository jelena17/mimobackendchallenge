using Application;
using Data.Repositories;
using Domain.Tests;
using Domain.Users;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Integration.Tests
{
    [TestClass]
    public class RepositoryIntegrationTests
    {
        private IUserRepository _userRepo;
        private ICourseRepository _courseRepo;

        [TestInitialize]
        public void Initialize()
        {
            var provider = DependencyConfiguration.Build();
            var context = provider.GetService<MimoDbContext>();
            _userRepo = new UserRepository(context);
            _courseRepo = new CourseRepository(context);
        }

        [TestMethod]
        public async Task Able_to_get_user_from_db()
        {
            var user = await _userRepo.Get(1);
            user.Should().NotBeNull();
            //should create expected and validate equivalent
        }

        [TestMethod]
        [Ignore]// increase userId
        public async Task Able_to_create_new_user()
        {
            var completedLessons = Enumerable.Range(1, 5)
                .Select(inx => new CompletedLesson(inx, Fakes.BuildInterval()))
                .ToList();

            var course = await _courseRepo.Get(1);

            var enrolledCourse = new Enrollement(
                course,
                completedLessons);

            var enrolledCourses = new[] { enrolledCourse };
            var user = new User(2, enrolledCourses);

            await _userRepo.Save(user);

            var userCreated = await _userRepo.Get(2);

            userCreated.Should().NotBeNull();
        }

        [TestMethod]
        public async Task Able_to_update_user()
        {
            var user = await _userRepo.Get(1);
            var lastCompleted = user
                .Enrollements
                .FirstOrDefault()
                .CompletedLessons.Last();

            var newLesson = new CompletedLesson(lastCompleted.Id + 1, Fakes.BuildInterval());
            user.CompleteLesson(newLesson);

            await _userRepo.Save(user);

            var userUpdated = await _userRepo.Get(1);

            user
                .Enrollements
                .FirstOrDefault()
                .CompletedLessons
                .Should()
                .HaveCount(lastCompleted.Id + 1);
        }
    }
}