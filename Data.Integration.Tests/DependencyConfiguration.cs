﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Data.Integration.Tests
{
    public static class DependencyConfiguration
    {
        public static ServiceProvider Build()
        {
            var services = new ServiceCollection();

            services
                .AddEntityFrameworkSqlite()
                .AddDbContext<MimoDbContext>(options =>
                  options.UseSqlite($"Data Source=./MimoDb-Integration.db"));
            return services.BuildServiceProvider();
        }
    }
}