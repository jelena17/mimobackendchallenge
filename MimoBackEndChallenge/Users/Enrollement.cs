﻿using Domain.Courses;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Users
{
    public class Enrollement
    {
        public Course Course { get; private set; }
        public List<CompletedLesson> CompletedLessons { get; private set; }

        public Enrollement(
            Course courseContent,
            List<CompletedLesson> completedLessons = null)
        {
            Course = courseContent;
            CompletedLessons = completedLessons
                ?? new List<CompletedLesson>();
        }

        private Enrollement()
        {
        }

        public bool IsLessonPartOfCourse(int lessonId)
            => Course.AllLessons.Any(lesson => lesson.Id == lessonId);

        // Here I made an assumption. If the lesson is completed before, update if the time of
        // completion is better.
        public void CompleteLesson(CompletedLesson lesson)
        {
            if (!IsLessonAlreadyCompleated(lesson.Id))
                AddLesson(lesson);
            else
            {
                var alreadyCompletedLesson =
                    GetCompletedLesson(lesson.Id);

                if (lesson.HasBetterCompletitionInterval(alreadyCompletedLesson))
                    UpdateCompletedLesson(lesson);
            }
        }

        public CompletedLesson GetCompletedLesson(int lessonId)
          => CompletedLessons.FirstOrDefault(lesson => lesson.Id == lessonId);

        public int CourseLessonsCount
            => Course.LessonsCount;

        public int CourseChaptersCount
           => Course.Chapters.Count();

        public IEnumerable<Chapter> CourseChapters
            => Course.Chapters;

        private bool IsLessonAlreadyCompleated(int lessonId)
           => CompletedLessons.Any(lesson => lesson.Id == lessonId);

        private void AddLesson(CompletedLesson lesson)
            => CompletedLessons.Add(lesson);

        private void UpdateCompletedLesson(CompletedLesson lesson)
        {
            var existinLesson = GetCompletedLesson(lesson.Id);
            CompletedLessons.Remove(existinLesson);
            CompletedLessons.Add(lesson);
        }
    }
}