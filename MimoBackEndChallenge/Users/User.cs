﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Users
{
    public class User : Entity
    {
        public IReadOnlyList<Enrollement> Enrollements => _enrollements;
        private List<Enrollement> _enrollements;

        public User(
            int userId,
            IEnumerable<Enrollement> enrolledCourses = null)
            : base(userId)
        {
            _enrollements = enrolledCourses?.ToList()
                ?? new List<Enrollement>();
        }

        private User()
        {
        }

        public void CompleteLesson(CompletedLesson lesson)
        {
            var correspondingEnrollment = _enrollements
                .FirstOrDefault(course => course.IsLessonPartOfCourse(lesson.Id));

            if (correspondingEnrollment == default)
                throw new ArgumentException("Lesson is not part of enrolled courses of user.");

            correspondingEnrollment.CompleteLesson(lesson);
        }
    }
}