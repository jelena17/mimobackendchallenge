﻿using System;

namespace Domain.Users
{
    public class DateInterval
    {
        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }

        private const string ErrorMessage
            = "Unable to create DateInterval. Start date can not be after end date.";

        public DateInterval(DateTime start, DateTime end)
        {
            if (start > end)
                throw new ArgumentException(ErrorMessage);

            Start = start;
            End = end;
        }

        public static DateInterval Create(DateTime start, DateTime end)
            => new DateInterval(start, end);

        private DateInterval()
        {
        }

        public double Duration
            => (End - Start).TotalMinutes;
    }
}