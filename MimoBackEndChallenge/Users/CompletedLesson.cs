﻿namespace Domain.Users
{
    public class CompletedLesson : Entity
    {
        public DateInterval CompletitionInterval { get; private set; }

        public CompletedLesson(int id, DateInterval completitionInterval)
            : base(id)
        {
            CompletitionInterval = completitionInterval;
        }

        private CompletedLesson()
        {
        }

        public bool HasBetterCompletitionInterval(CompletedLesson other)
        {
            return this.CompletitionInterval.Duration
                < other.CompletitionInterval.Duration;
        }
    }
}