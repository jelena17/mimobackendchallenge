﻿namespace Domain.Achievements
{
    public class Achievement
    {
        // I added course name as it was not clear which achievement is linked to which course if
        // user is enrolled in multiple courses.
        public string CourseName { get; private set; }

        public Objective Objective { get; private set; }
        public bool IsCompleted { get; private set; }
        public int? Progress { get; private set; }
        public bool IsInProgress => Progress != null;

        private Achievement(
           string courseName,
           Objective objective,
           bool isCompleted,
           int? proggres = null)
        {
            CourseName = courseName;
            Objective = objective;
            IsCompleted = isCompleted;
            Progress = proggres;
        }

        public static Achievement Completed(string courseName, Objective objective)
            => new Achievement(courseName, objective, true);

        public static Achievement InProgress(string courseName, Objective objective, int proggres)
            => new Achievement(courseName, objective, false, proggres);
    }
}