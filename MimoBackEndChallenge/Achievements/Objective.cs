﻿namespace Domain.Achievements
{
    public enum Objective
    {
        Five_Lessons_Completed,
        TwentyFive_Lessons_Completed,
        Fifty_Lessons_Completed,
        One_Chapter_Completed,
        Five_Chapters_Completed,
        Course_Completed
    }
}