﻿using Domain.Users;
using System.Collections.Generic;

namespace Domain.Achievements
{
    public interface IAchievementCalculationStrategy
    {
        IEnumerable<Achievement> GetAchievements(User user);
    }
}