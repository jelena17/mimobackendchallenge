﻿using Domain.Users;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Achievements
{
    public class LessonCountBasedCalculationStrategy
        : IAchievementCalculationStrategy
    {
        public IEnumerable<Achievement> GetAchievements(User user)
        {
            var allAchievements = new List<Achievement>();

            foreach (var course in user.Enrollements)
            {
                var courseAchievements = GetAchievementsForCourse(course);
                allAchievements.AddRange(courseAchievements);
            }
            return allAchievements;
        }

        private IEnumerable<Achievement> GetAchievementsForCourse(
            Enrollement course)
        {
            string courseName = course.Course.Name;
            int completedLessonCount = course.CompletedLessons.Count();
            Dictionary<Objective, int> objectivesConfig = ConfigureApplicableObjectives(course);

            foreach (var objective in objectivesConfig.Keys)
            {
                var requiredLessonCompletionCount = objectivesConfig[objective];

                var achievement = GetObjectiveAchievement(
                    completedLessonCount,
                    requiredLessonCompletionCount,
                    objective,
                    courseName);

                yield return achievement;

                if (achievement.IsInProgress)
                    break;
            }
        }

        //Intention of this method is to provide count of lessons need to be completed, to get achievement
        private static Dictionary<Objective, int> ConfigureApplicableObjectives(Enrollement enrollment)
        {
            var config = new Dictionary<Objective, int>();

            config.Add(Objective.Five_Lessons_Completed, 5);

            if (enrollment.CourseLessonsCount >= 25)
                config.Add(Objective.TwentyFive_Lessons_Completed, 25);

            if (enrollment.CourseLessonsCount >= 50)
                config.Add(Objective.Fifty_Lessons_Completed, 50);

            var oneChapterLessonCount = enrollment
               .CourseChapters
               .First()
               .LessonCount;

            config.Add(Objective.One_Chapter_Completed, oneChapterLessonCount);

            if (enrollment.CourseChaptersCount >= 5)
            {
                var fiveChapterLessonCount = enrollment.CourseChapters
                 .Take(5)
                 .SelectMany(l => l.Lessons)
                 .Count();

                config.Add(Objective.Five_Chapters_Completed, fiveChapterLessonCount);
            }

            config.Add(Objective.Course_Completed, enrollment.CourseLessonsCount);
            return config;
        }

        private Achievement GetObjectiveAchievement(
            int completedLessonCount,
            int objectiveLessonCount,
            Objective objective,
            string courseName)
        {
            if (completedLessonCount < objectiveLessonCount)
                return Achievement.InProgress(courseName, objective, completedLessonCount);
            else
                return Achievement.Completed(courseName, objective);
        }
    }
}