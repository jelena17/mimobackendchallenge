﻿namespace Domain.Courses
{
    public class Lesson : Entity
    {
        public int Index { get; private set; }
        public string Name { get; private set; }

        public Lesson(int index, string name)
        {
            Index = index;
            Name = name;
        }

        public Lesson(int id, int index, string name)
            : base(id)
        {
            Index = index;
            Name = name;
        }

        private Lesson()
        {
        }
    }
}