﻿using System.Collections.Generic;
using System.Linq;

namespace Domain.Courses
{
    public class Course : Entity
    {
        public string Name { get; private set; }

        public IReadOnlyList<Chapter> Chapters
            => _chapters.OrderBy(c => c.Index).ToList();

        private List<Chapter> _chapters;

        public Course(
            string name,
            IEnumerable<Chapter> chapters = null)
        {
            Name = name;
            _chapters = chapters?.ToList() ?? new List<Chapter>();
        }

        public Course(
            int id,
            string name,
            IEnumerable<Chapter> chapters = null)
            : base(id)
        {
            Name = name;
            _chapters = chapters?.ToList() ?? new List<Chapter>();
        }

        private Course()
        {
        }

        public IEnumerable<Lesson> AllLessons
            => Chapters.SelectMany(l => l.Lessons);

        public int LessonsCount
          => AllLessons.Count();
    }
}