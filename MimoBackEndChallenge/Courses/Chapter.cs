﻿using System.Collections.Generic;
using System.Linq;

namespace Domain.Courses
{
    public class Chapter : Entity
    {
        public int Index { get; private set; }
        public string Name { get; private set; }

        public IReadOnlyList<Lesson> Lessons
            => _lessons.OrderBy(l => l.Index).ToList();

        private List<Lesson> _lessons;

        public Chapter(
           int index,
           string name,
           IEnumerable<Lesson> lessons = null)
        {
            Index = index;
            Name = name;
            _lessons = lessons?.ToList() ?? new List<Lesson>();
        }

        public Chapter(
         int id,
         int index,
         string name,
         IEnumerable<Lesson> lessons = null)
            : base(id)
        {
            Index = index;
            Name = name;
            _lessons = lessons?.ToList() ?? new List<Lesson>();
        }

        private Chapter()
        {
        }

        public int LessonCount
            => Lessons.Count();
    }
}