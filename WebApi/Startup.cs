using Application;
using Data;
using Data.Repositories;
using Domain.Achievements;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;
using WebApi.Authentication;

namespace WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvc(opt =>
                    opt.Filters.Add(new ExceptionResultFilter()));

            services
                .AddControllers()
                .AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.Converters
                    .Add(new JsonStringEnumConverter());
            });

            //dependencies
            services.AddScoped<ICourseRepository, CourseRepository>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<ITokenProvider, TokenProvider>();
            services.AddScoped<IAchievementCalculationStrategy, LessonCountBasedCalculationStrategy>();
            services.AddScoped<IUserRepository, UserRepository>();

            //settings
            var tokenSettings = Configuration
                .GetSection("JwtSettings")
                .Get<JwtSettings>();

            services.AddSingleton(tokenSettings);
            //usernames and passwords
            services.AddSingleton(s =>
            new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                { "User","User"},
                { "Admin","Admin"}
             });

            services.AddEntityFrameworkSqlite()
                .AddDbContext<MimoDbContext>(options =>
                  options.UseSqlite($"Data Source=./MimoDb.db"));

            services.AddSwaggerGen(opt =>
           {
               opt.SwaggerDoc("v1", new OpenApiInfo { Title = "WebApi", Version = "v1" });

               opt.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
               {
                   Name = "Authorization",
                   In = ParameterLocation.Header,
                   Type = SecuritySchemeType.ApiKey,
               });

               opt.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Id = "Bearer",
                                Type = ReferenceType.SecurityScheme,
                            }
                        },
                        new string[] { }
                    }
                });
           });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                     .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme,
                         opt =>
                         {
                             opt.SaveToken = true;
                             opt.TokenValidationParameters = new TokenValidationParameters
                             {
                                 ValidateIssuer = false,
                                 ValidateLifetime = true,
                                 ValidateAudience = false,
                                 RequireExpirationTime = false,
                                 ValidateIssuerSigningKey = true,
                                 IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenSettings.Key)),
                             };
                         });

            services.AddAuthorization(opt =>
            {
                opt.AddPolicy(Roles.Admin,
                    policy =>
                    {
                        policy.RequireAuthenticatedUser();
                        policy.RequireRole(Roles.Admin);
                    }
                    );
                opt.AddPolicy(Roles.User,
                  policy =>
                  {
                      policy.RequireAuthenticatedUser();
                      policy.RequireRole(Roles.User);
                  });
                opt.AddPolicy(Roles.UserOrAdmin,
                 policy =>
                 {
                     policy.RequireAuthenticatedUser();
                     policy.RequireRole(Roles.UserOrAdmin);
                 });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebApi v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}