﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApi.Authentication;
using WebApi.Authentication.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class Authentication : ControllerBase
    {
        // this endpoint could be sitting in other auth dedicated Api
        //for simplicity it is here.
        private readonly IAuthenticationService _service;

        public Authentication(IAuthenticationService service)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<IActionResult> Authenticate(
            [FromBody] AuthenticationRequest request)
        {
            var response = _service.Authenticate(request);
            return Ok(response);
        }
    }
}