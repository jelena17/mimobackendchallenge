﻿using Application;
using Application.Commands;
using Application.Queries;
using Domain.Achievements;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Authentication;

namespace WebApi.Controllers
{
    [ApiController]
    [Authorize(Roles = Roles.UserOrAdmin)]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _repository;
        private readonly IAchievementCalculationStrategy _achievementCalculationStrategy;

        public UserController(IUserRepository repository,
            IAchievementCalculationStrategy achievementCalculationStrategy)
        {
            _repository = repository;
            _achievementCalculationStrategy = achievementCalculationStrategy;
        }

        [HttpPost]
        [Route("api/user/lesson")]
        public async Task<IActionResult> CompleteLesson(
            [FromBody] CompleteLessonRequest request)
        {
            ICommand<CompleteLessonRequest> command =
                new UserCompletesLessonCommand(_repository);

            request.SetUser(GetUserId());
            await command.Execute(request);

            return Created("", request);
        }

        [HttpGet]
        [Route("api/user/achievements")]
        public async Task<IActionResult> GetAchievements()
        {
            IQuery<int, IEnumerable<AchivementResponse>> query =
                new UserAchievementQuery(_repository, _achievementCalculationStrategy);

            var userId = GetUserId();
            var response = await query.Execute(userId);

            return Ok(response);
        }

        private int GetUserId()
        {
            var idString = User.FindFirst(UserClaims.UserId).Value;
            if (!Int32.TryParse(idString, out int id))
                throw new ArgumentException("Invalid token, UserId missing.");
            return id;
        }
    }
}