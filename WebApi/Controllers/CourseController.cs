﻿using Application;
using Application.Commands;
using Application.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApi.Authentication;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CourseController : ControllerBase
    {
        private readonly ICourseRepository _repo;

        public CourseController(ICourseRepository repo)
        {
            _repo = repo;
        }

        [HttpPost]
        [Authorize(Roles = Roles.Admin)]
        public async Task<IActionResult> CreateCourse(CreateCourseRequest request)
        {
            var command = new CreateCourseCommand(_repo);
            await command.Execute(request);

            return Created("GetCourse", request);
        }

        [HttpGet]
        [Authorize(Roles = Roles.UserOrAdmin)]
        public async Task<IActionResult> GetCourse(int id)
        {
            //should be in GetCourseByIdQuery,
            //but for simplicity mapping into response is not implemented.
            var course = await _repo.Get(id);
            return Ok(course);
        }
    }
}