﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace WebApi
{
    public class ExceptionResultFilter :
       IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            context.Result = CreateResult(context.Exception);
        }

        //not implemented fully as for simplicity I mostly use one exception type
        private IActionResult CreateResult(Exception exception)
        {
            if (exception.GetType() == typeof(ArgumentException))
                return new BadRequestObjectResult(exception.Message);
            else
                return new ObjectResult(exception.Message) { StatusCode = 500 };
        }
    }
}