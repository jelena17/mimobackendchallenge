﻿using WebApi.Authentication.Models;
using WebApi.Controllers;

namespace WebApi.Authentication
{
    public interface IAuthenticationService
    {
        TokenResponse Authenticate(AuthenticationRequest request);
    }
}