﻿namespace WebApi.Authentication
{
    public static class Roles
    {
        public const string Admin = "Admin";

        public const string User = "User";
        public const string UserOrAdmin = "User,Admin";
    }
}