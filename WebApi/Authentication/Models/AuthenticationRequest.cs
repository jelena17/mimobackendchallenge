﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Authentication.Models
{
    public class AuthenticationRequest
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }
    }
}