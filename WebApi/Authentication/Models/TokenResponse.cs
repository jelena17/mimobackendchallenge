﻿using System;

namespace WebApi.Controllers
{
    public class TokenResponse
    {
        public string Token { get; set; }
        public string Type => "Bearer";
        public DateTime? ExpitesAt { get; set; }
    }
}