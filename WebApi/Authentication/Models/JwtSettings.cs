﻿namespace WebApi
{
    public class JwtSettings
    {
        public string Key { get; set; }
        public int ExpirationInMins { get; set; }
    }
}