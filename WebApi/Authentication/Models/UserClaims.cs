﻿namespace WebApi.Authentication
{
    public static class UserClaims
    {
        public const string UserId = "UserId";
        public const string UserAccess = "UserAcess";
    }
}