﻿using System;
using System.Collections.Generic;
using WebApi.Authentication.Models;
using WebApi.Controllers;

namespace WebApi.Authentication
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly Dictionary<string, string> _users;
        private readonly ITokenProvider _tokenProvider;

        public AuthenticationService(
            Dictionary<string, string> users,
            ITokenProvider tokenProvider)
        {
            _users = users;
            _tokenProvider = tokenProvider;
        }

        public TokenResponse Authenticate(AuthenticationRequest request)
        {
            if (!_users.TryGetValue(request.UserName, out string password))
                throw new ArgumentException("Can not authenticate user. User not found.");

            if (password != request.Password)// no encryption :D
                throw new ArgumentException("Can not authenticate user. Invalid password");

            return _tokenProvider.Provide(request.UserName);
        }
    }
}