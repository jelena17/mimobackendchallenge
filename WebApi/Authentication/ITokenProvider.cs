﻿using WebApi.Controllers;

namespace WebApi.Authentication
{
    public interface ITokenProvider
    {
        TokenResponse Provide(string username);
    }
}