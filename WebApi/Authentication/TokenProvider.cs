﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using WebApi.Controllers;

namespace WebApi.Authentication
{
    public class TokenProvider : ITokenProvider
    {
        private readonly JwtSettings _settings;

        public TokenProvider(JwtSettings settings)
        {
            _settings = settings;
        }

        public TokenResponse Provide(string userName)
        {
            SecurityTokenDescriptor tokenDescriptor =
                CreateTokenDescriptor(userName);

            string token = GenerateToken(tokenDescriptor);

            return new TokenResponse
            {
                Token = token,
                ExpitesAt = tokenDescriptor.Expires
            };
        }

        private SecurityTokenDescriptor CreateTokenDescriptor(string userName)
        {
            var key = Encoding.UTF8.GetBytes(_settings.Key);

            return new SecurityTokenDescriptor
            {
                Subject = CreateClaimsIdentity(userName),
                Expires = DateTime.UtcNow.AddMinutes(_settings.ExpirationInMins),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
            };
        }

        private static string GenerateToken(SecurityTokenDescriptor tokenDescriptor)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken tokenModel = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(tokenModel);
        }

        private ClaimsIdentity CreateClaimsIdentity(string userName)
            => new ClaimsIdentity(CreateClaims(userName));

        private IEnumerable<Claim> CreateClaims(string userName)
        {
            //hard-code claims per user and admin
            if (userName.Equals("User", StringComparison.OrdinalIgnoreCase))
            {
                yield return new Claim(ClaimTypes.Role, Roles.User);
                yield return new Claim(UserClaims.UserId, "1");
            }
            else
            {
                yield return new Claim(ClaimTypes.Role, Roles.Admin);
                yield return new Claim(UserClaims.UserId, "2");
            }
        }
    }
}