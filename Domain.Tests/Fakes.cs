﻿using Domain.Courses;
using Domain.Users;
using System;
using System.Linq;

namespace Domain.Tests
{
    public static class Fakes
    {
        public static Course BuildCourse(
            string name = "C#",
            int chapterCount = 10,
            int lessonCountPerChapter = 10)
        {
            var chapters = Enumerable.Range(1, chapterCount)
              .Select(x => new Chapter(
                  id: x,
                  index: x,
                  name: $"Chapter:{x}",
                  lessons: Enumerable.Range(lessonCountPerChapter * (x - 1), lessonCountPerChapter)
                    .Select(inx => new Lesson(inx, inx, $"Lesson:{inx}"))));

            return new Course(name, chapters);
        }

        public static DateInterval BuildInterval()
            => DateInterval.Create(DateTime.UtcNow.AddMinutes(-30), DateTime.UtcNow);

        public static User BuildUser(
            int completedLessonsCount = 50,
            int chapterCount = 10,
            int lessonCountPerChapter = 10,
            int userId = 1)
        {
            var completedLessons = Enumerable.Range(1, completedLessonsCount)
                .Select(inx => new CompletedLesson(inx, BuildInterval()))
                .ToList();

            var enrolledCourse = new Enrollement(
                BuildCourse(
                    chapterCount: chapterCount,
                    lessonCountPerChapter: lessonCountPerChapter),
                completedLessons);

            var enrolledCourses = new[] { enrolledCourse };
            return new User(userId, enrolledCourses);
        }
    }
}