﻿using Domain.Achievements;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Domain.Tests
{
    [TestClass]
    [TestCategory("Unit")]
    public class LessonCountBasedCalculationStrategyTests
    {
        private LessonCountBasedCalculationStrategy _strategy;

        [TestInitialize]
        public void Inicialize()
        {
            _strategy = new LessonCountBasedCalculationStrategy();
        }

        [TestMethod]
        public void Able_to_get_user_achievements_last_achievement_in_progress()
        {
            var user = Fakes.BuildUser(
                chapterCount: 10,
                lessonCountPerChapter: 50,
                completedLessonsCount: 40);

            var expected = new[] {
                Achievement.Completed("C#", Objective.Five_Lessons_Completed),
                Achievement.Completed("C#", Objective.TwentyFive_Lessons_Completed),
                Achievement.InProgress("C#", Objective.Fifty_Lessons_Completed,40),
            };

            var result = _strategy.GetAchievements(user);

            result
                .Should()
                .BeEquivalentTo(expected);
        }

        [TestMethod]
        public void Able_to_get_user_achievements_course_has_3_chapters_in_total()
        {
            var user = Fakes.BuildUser(
                chapterCount: 3,
                lessonCountPerChapter: 10,
                completedLessonsCount: 30);

            var expected = new[] {
                Achievement.Completed("C#", Objective.Five_Lessons_Completed),
                Achievement.Completed("C#", Objective.TwentyFive_Lessons_Completed),
                Achievement.Completed("C#", Objective.One_Chapter_Completed),
                Achievement.Completed("C#", Objective.Course_Completed),
            };

            var result = _strategy.GetAchievements(user);

            result
                .Should()
                .BeEquivalentTo(expected);
        }

        [TestMethod]
        public void Able_to_get_user_with_all_achievements()
        {
            var user = Fakes.BuildUser(
                chapterCount: 5,
                lessonCountPerChapter: 20,
                completedLessonsCount: 100);

            var expected = new[] {
                Achievement.Completed("C#", Objective.Five_Lessons_Completed),
                Achievement.Completed("C#", Objective.TwentyFive_Lessons_Completed),
                Achievement.Completed("C#", Objective.Fifty_Lessons_Completed),
                Achievement.Completed("C#", Objective.One_Chapter_Completed),
                Achievement.Completed("C#", Objective.Five_Chapters_Completed),
                Achievement.Completed("C#", Objective.Course_Completed),
            };

            var result = _strategy.GetAchievements(user);

            result
                .Should()
                .BeEquivalentTo(expected);
        }
    }
}