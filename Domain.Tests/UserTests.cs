﻿using Domain.Users;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace Domain.Tests
{
    [TestClass]
    [TestCategory("Unit")]
    public class UserTests
    {
        [TestMethod]
        public void Able_to_add_completed_lesson()
        {
            var user = Fakes.BuildUser(
                lessonCountPerChapter: 50,
                completedLessonsCount: 40);

            var newInterval = DateInterval.Create(
                DateTime.UtcNow.AddMinutes(-3),
                DateTime.UtcNow);

            user.CompleteLesson(new CompletedLesson(41, newInterval));

            user.Enrollements
                .First().CompletedLessons
                .Should()
                .HaveCount(41);
        }

        [TestMethod]
        public void Able_to_update_completed_lesson_due_to_better_interval_of_comletion()
        {
            var user = Fakes.BuildUser();

            var newInterval = DateInterval.Create(
                DateTime.UtcNow.AddMinutes(-3),
                DateTime.UtcNow);

            user.CompleteLesson(new CompletedLesson(1, newInterval));

            user
                .Enrollements
                .First()
                .GetCompletedLesson(1)
                .CompletitionInterval
                .Should()
                .BeEquivalentTo(newInterval);

            user.Enrollements
                .First().CompletedLessons
                .Should()
                .HaveCount(50);
        }

        [TestMethod]
        public void Able_to_notupdate_completed_lesson_worse_interval_of_comletion()
        {
            var user = Fakes.BuildUser();

            var newInterval = DateInterval.Create(
                DateTime.UtcNow.AddMinutes(-40),
                DateTime.UtcNow);

            user.CompleteLesson(new CompletedLesson(1, newInterval));

            user
                .Enrollements
                .First()
                .GetCompletedLesson(1)
                .CompletitionInterval
                .Should()
                .NotBeEquivalentTo(newInterval);

            user.Enrollements
                .First().CompletedLessons
                .Should()
                .HaveCount(50);
        }
    }
}