﻿using Domain.Courses;
using Domain.Users;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class MimoDbContext : DbContext
    {
        public DbSet<Course> Courses { get; set; }
        public DbSet<User> Users { get; set; }

        public MimoDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Course>()
                .HasMany(c => c.Chapters)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder
               .Entity<Course>()
               .Ignore(c => c.AllLessons)
               .Ignore(c => c.LessonsCount);

            modelBuilder
               .Entity<Chapter>()
               .HasMany(c => c.Lessons)
               .WithOne()
               .OnDelete(DeleteBehavior.Cascade);

            modelBuilder
                .Entity<Chapter>()
                .Ignore(c => c.LessonCount);

            modelBuilder
               .Entity<Lesson>()
               .SeedLessons();

            modelBuilder
               .Entity<Chapter>()
               .SeedChapters();

            modelBuilder
               .Entity<Course>()
               .SeedCourses();

            modelBuilder.Entity<User>(x =>
            {
                x.SeedUsers();
                x.OwnsMany(s => s.Enrollements)
                 .Ignore(s => s.CompletedLessons)
                 .Ignore(s => s.CourseChapters)
                 .Ignore(s => s.CourseChapters)
                 .ToTable("EnrolledCourses")
                 .HasKey("Id");

                var builder = x.OwnsMany(e => e.Enrollements);
                builder.SeedEnrollments();
                builder.HasOne(c => c.Course);
                builder.OwnsMany(l => l.CompletedLessons, cl =>
                   {
                       cl.Property(p => p.Id).HasColumnName("LessonId");
                       cl.OwnsOne(p => p.CompletitionInterval, c =>
                       {
                           c.Property(p => p.Start).HasColumnName("LessonStarted");
                           c.Property(p => p.End).HasColumnName("LessonEnded");
                           c.SeedInterval();
                       }).SeedLessonsCompleted();
                       cl.ToTable("CompletedLessons");
                   }); ;
            });
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
            //optionsBuilder.UseSqlite("Filename=./MimoDb.db");
        }
    }
}