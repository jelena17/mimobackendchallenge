﻿using Application;
using Domain.Users;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class UserRepository
        : IUserRepository
    {
        private readonly MimoDbContext _db;

        public UserRepository(MimoDbContext db)
        {
            _db = db;
        }

        public async Task<User> Get(int userId)
        {
            var user = await _db.Users
                .Include(u => u.Enrollements)
                .ThenInclude(u => u.CompletedLessons)
                .ThenInclude(x => x.CompletitionInterval)
                .Include(x => x.Enrollements)
                .ThenInclude(x => x.Course)
                .ThenInclude(x => x.Chapters)
                .ThenInclude(x => x.Lessons)
                .FirstOrDefaultAsync(user => user.Id == userId);

            if (user == default)
                throw new ArgumentException("User not found");

            return user;
        }

        public Task Save(User user)
        {
            if (_db.Entry(user).State == EntityState.Detached)
            {
                _db.Users.Add(user);
                foreach (var enrollment in user.Enrollements)
                    _db.Entry(enrollment.Course).State = EntityState.Detached;
            }
            else
            {
                _db.Users.Update(user);
                foreach (var enrollment in user.Enrollements)
                    _db.Entry(enrollment.Course).State = EntityState.Detached;
            }
            return _db.SaveChangesAsync();
        }
    }
}