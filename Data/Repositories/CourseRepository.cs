﻿using Application;
using Domain.Courses;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class CourseRepository
        : ICourseRepository
    {
        private readonly MimoDbContext _db;

        public CourseRepository(MimoDbContext db)
        {
            _db = db;
        }

        public async Task<Course> Get(int id)
        {
            var course = await _db.Courses
                .Include(c => c.Chapters)
                .ThenInclude(c => c.Lessons)
                .AsNoTracking()
                .FirstOrDefaultAsync(c => c.Id == id);

            if (course == default)
                throw new ArgumentException("Course not found");
            //should throw custom data_access_layer exception, but have no time :)

            return course;
        }

        public async Task Create(Course course)
        {
            var existingCourse = await _db.Courses
                .Where(x => x.Name == course.Name)
                .SingleOrDefaultAsync();

            if (existingCourse != default)
                throw new ArgumentException("Course already exists.");

            _db.Courses.Add(course);
            await _db.SaveChangesAsync();
        }
    }
}