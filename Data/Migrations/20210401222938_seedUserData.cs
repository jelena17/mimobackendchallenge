﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class seedUserData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Users",
                column: "Id",
                value: 1);

            migrationBuilder.InsertData(
                table: "EnrolledCourses",
                columns: new[] { "Id", "CourseId", "UserId" },
                values: new object[] { 1, 1, 1 });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 1, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(5604), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(5509) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 28, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9240), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9239) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 29, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9242), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9241) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 30, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9244), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9243) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 31, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9246), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9245) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 32, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9248), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9247) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 33, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9250), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9249) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 34, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9252), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9251) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 35, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9254), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9253) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 36, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9256), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9255) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 37, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9258), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9257) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 38, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9260), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9259) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 39, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9261), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9261) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 40, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9263), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9262) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 41, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9265), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9264) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 42, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9267), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9266) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 43, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9269), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9268) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 44, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9271), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9270) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 45, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9273), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9272) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 46, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9275), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9274) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 47, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9277), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9276) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 48, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9279), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9278) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 27, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9238), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9238) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 26, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9236), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9236) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 25, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9235), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9234) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 24, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9233), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9232) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 2, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9186), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9175) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 3, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9191), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9190) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 4, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9193), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9192) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 5, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9195), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9194) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 6, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9197), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9196) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 7, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9199), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9199) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 8, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9201), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9201) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 9, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9203), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9203) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 10, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9205), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9205) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 11, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9207), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9206) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 49, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9281), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9280) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 12, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9209), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9208) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 14, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9213), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9212) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 15, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9215), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9214) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 16, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9217), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9216) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 17, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9219), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9218) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 18, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9221), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9220) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 19, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9223), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9222) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 20, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9225), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9224) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 21, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9227), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9226) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 22, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9229), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9228) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 23, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9231), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9230) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 13, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9211), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9210) });

            migrationBuilder.InsertData(
                table: "CompletedLessons",
                columns: new[] { "EnrollementId", "LessonId", "LessonEnded", "LessonStarted" },
                values: new object[] { 1, 50, new DateTime(2021, 4, 1, 22, 29, 38, 115, DateTimeKind.Utc).AddTicks(9283), new DateTime(2021, 4, 1, 21, 59, 38, 115, DateTimeKind.Utc).AddTicks(9282) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 3 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 4 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 5 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 6 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 7 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 8 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 9 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 10 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 11 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 12 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 13 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 14 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 15 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 16 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 17 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 18 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 19 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 20 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 21 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 22 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 23 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 24 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 25 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 26 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 27 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 28 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 29 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 30 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 31 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 32 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 33 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 34 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 35 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 36 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 37 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 38 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 39 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 40 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 41 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 42 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 43 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 44 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 45 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 46 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 47 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 48 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 49 });

            migrationBuilder.DeleteData(
                table: "CompletedLessons",
                keyColumns: new[] { "EnrollementId", "LessonId" },
                keyValues: new object[] { 1, 50 });

            migrationBuilder.DeleteData(
                table: "EnrolledCourses",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
