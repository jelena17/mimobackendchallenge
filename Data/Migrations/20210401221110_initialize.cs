﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class initialize : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Courses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Chapter",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Index = table.Column<int>(type: "INTEGER", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    CourseId = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chapter", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Chapter_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EnrolledCourses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CourseId = table.Column<int>(type: "INTEGER", nullable: true),
                    UserId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnrolledCourses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EnrolledCourses_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EnrolledCourses_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Lesson",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Index = table.Column<int>(type: "INTEGER", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    ChapterId = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lesson", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Lesson_Chapter_ChapterId",
                        column: x => x.ChapterId,
                        principalTable: "Chapter",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompletedLessons",
                columns: table => new
                {
                    LessonId = table.Column<int>(type: "INTEGER", nullable: false),
                    EnrollementId = table.Column<int>(type: "INTEGER", nullable: false),
                    LessonStarted = table.Column<DateTime>(type: "TEXT", nullable: true),
                    LessonEnded = table.Column<DateTime>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompletedLessons", x => new { x.EnrollementId, x.LessonId });
                    table.ForeignKey(
                        name: "FK_CompletedLessons_EnrolledCourses_EnrollementId",
                        column: x => x.EnrollementId,
                        principalTable: "EnrolledCourses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "C#" });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "Id", "Name" },
                values: new object[] { 2, "Swift" });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "Id", "Name" },
                values: new object[] { 3, "JavaScript" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 1, 1, 1, "Chapter:1" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 28, 3, 28, "Chapter:28" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 27, 3, 27, "Chapter:27" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 26, 3, 26, "Chapter:26" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 25, 3, 25, "Chapter:25" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 24, 3, 24, "Chapter:24" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 23, 3, 23, "Chapter:23" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 22, 3, 22, "Chapter:22" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 21, 3, 21, "Chapter:21" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 20, 2, 20, "Chapter:20" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 19, 2, 19, "Chapter:19" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 18, 2, 18, "Chapter:18" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 17, 2, 17, "Chapter:17" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 16, 2, 16, "Chapter:16" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 15, 2, 15, "Chapter:15" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 14, 2, 14, "Chapter:14" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 13, 2, 13, "Chapter:13" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 12, 2, 12, "Chapter:12" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 11, 2, 11, "Chapter:11" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 10, 1, 10, "Chapter:10" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 9, 1, 9, "Chapter:9" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 8, 1, 8, "Chapter:8" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 7, 1, 7, "Chapter:7" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 6, 1, 6, "Chapter:6" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 5, 1, 5, "Chapter:5" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 4, 1, 4, "Chapter:4" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 3, 1, 3, "Chapter:3" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 2, 1, 2, "Chapter:2" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 29, 3, 29, "Chapter:29" });

            migrationBuilder.InsertData(
                table: "Chapter",
                columns: new[] { "Id", "CourseId", "Index", "Name" },
                values: new object[] { 30, 3, 30, "Chapter:30" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 1, 1, 1, "Lesson:1" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 204, 21, 204, "Lesson:204" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 203, 21, 203, "Lesson:203" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 202, 21, 202, "Lesson:202" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 201, 21, 201, "Lesson:201" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 200, 20, 200, "Lesson:200" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 199, 20, 199, "Lesson:199" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 205, 21, 205, "Lesson:205" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 198, 20, 198, "Lesson:198" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 196, 20, 196, "Lesson:196" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 195, 20, 195, "Lesson:195" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 194, 20, 194, "Lesson:194" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 193, 20, 193, "Lesson:193" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 192, 20, 192, "Lesson:192" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 191, 20, 191, "Lesson:191" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 197, 20, 197, "Lesson:197" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 206, 21, 206, "Lesson:206" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 207, 21, 207, "Lesson:207" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 208, 21, 208, "Lesson:208" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 223, 23, 223, "Lesson:223" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 222, 23, 222, "Lesson:222" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 221, 23, 221, "Lesson:221" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 220, 22, 220, "Lesson:220" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 219, 22, 219, "Lesson:219" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 218, 22, 218, "Lesson:218" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 217, 22, 217, "Lesson:217" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 216, 22, 216, "Lesson:216" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 215, 22, 215, "Lesson:215" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 214, 22, 214, "Lesson:214" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 213, 22, 213, "Lesson:213" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 212, 22, 212, "Lesson:212" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 211, 22, 211, "Lesson:211" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 210, 21, 210, "Lesson:210" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 209, 21, 209, "Lesson:209" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 190, 19, 190, "Lesson:190" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 189, 19, 189, "Lesson:189" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 188, 19, 188, "Lesson:188" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 187, 19, 187, "Lesson:187" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 167, 17, 167, "Lesson:167" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 166, 17, 166, "Lesson:166" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 165, 17, 165, "Lesson:165" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 164, 17, 164, "Lesson:164" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 163, 17, 163, "Lesson:163" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 162, 17, 162, "Lesson:162" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 161, 17, 161, "Lesson:161" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 160, 16, 160, "Lesson:160" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 159, 16, 159, "Lesson:159" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 158, 16, 158, "Lesson:158" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 157, 16, 157, "Lesson:157" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 156, 16, 156, "Lesson:156" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 155, 16, 155, "Lesson:155" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 154, 16, 154, "Lesson:154" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 153, 16, 153, "Lesson:153" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 168, 17, 168, "Lesson:168" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 224, 23, 224, "Lesson:224" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 169, 17, 169, "Lesson:169" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 171, 18, 171, "Lesson:171" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 186, 19, 186, "Lesson:186" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 185, 19, 185, "Lesson:185" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 184, 19, 184, "Lesson:184" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 183, 19, 183, "Lesson:183" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 182, 19, 182, "Lesson:182" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 181, 19, 181, "Lesson:181" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 180, 18, 180, "Lesson:180" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 179, 18, 179, "Lesson:179" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 178, 18, 178, "Lesson:178" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 177, 18, 177, "Lesson:177" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 176, 18, 176, "Lesson:176" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 175, 18, 175, "Lesson:175" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 174, 18, 174, "Lesson:174" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 173, 18, 173, "Lesson:173" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 172, 18, 172, "Lesson:172" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 170, 17, 170, "Lesson:170" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 152, 16, 152, "Lesson:152" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 225, 23, 225, "Lesson:225" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 227, 23, 227, "Lesson:227" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 279, 28, 279, "Lesson:279" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 278, 28, 278, "Lesson:278" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 277, 28, 277, "Lesson:277" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 276, 28, 276, "Lesson:276" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 275, 28, 275, "Lesson:275" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 274, 28, 274, "Lesson:274" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 280, 28, 280, "Lesson:280" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 273, 28, 273, "Lesson:273" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 271, 28, 271, "Lesson:271" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 270, 27, 270, "Lesson:270" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 269, 27, 269, "Lesson:269" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 268, 27, 268, "Lesson:268" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 267, 27, 267, "Lesson:267" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 266, 27, 266, "Lesson:266" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 272, 28, 272, "Lesson:272" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 281, 29, 281, "Lesson:281" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 282, 29, 282, "Lesson:282" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 283, 29, 283, "Lesson:283" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 298, 30, 298, "Lesson:298" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 297, 30, 297, "Lesson:297" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 296, 30, 296, "Lesson:296" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 295, 30, 295, "Lesson:295" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 294, 30, 294, "Lesson:294" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 293, 30, 293, "Lesson:293" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 292, 30, 292, "Lesson:292" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 291, 30, 291, "Lesson:291" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 290, 29, 290, "Lesson:290" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 289, 29, 289, "Lesson:289" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 288, 29, 288, "Lesson:288" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 287, 29, 287, "Lesson:287" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 286, 29, 286, "Lesson:286" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 285, 29, 285, "Lesson:285" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 284, 29, 284, "Lesson:284" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 265, 27, 265, "Lesson:265" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 264, 27, 264, "Lesson:264" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 263, 27, 263, "Lesson:263" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 262, 27, 262, "Lesson:262" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 242, 25, 242, "Lesson:242" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 241, 25, 241, "Lesson:241" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 240, 24, 240, "Lesson:240" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 239, 24, 239, "Lesson:239" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 238, 24, 238, "Lesson:238" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 237, 24, 237, "Lesson:237" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 236, 24, 236, "Lesson:236" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 235, 24, 235, "Lesson:235" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 234, 24, 234, "Lesson:234" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 233, 24, 233, "Lesson:233" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 232, 24, 232, "Lesson:232" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 231, 24, 231, "Lesson:231" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 230, 23, 230, "Lesson:230" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 229, 23, 229, "Lesson:229" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 228, 23, 228, "Lesson:228" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 243, 25, 243, "Lesson:243" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 226, 23, 226, "Lesson:226" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 244, 25, 244, "Lesson:244" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 246, 25, 246, "Lesson:246" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 261, 27, 261, "Lesson:261" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 260, 26, 260, "Lesson:260" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 259, 26, 259, "Lesson:259" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 258, 26, 258, "Lesson:258" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 257, 26, 257, "Lesson:257" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 256, 26, 256, "Lesson:256" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 255, 26, 255, "Lesson:255" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 254, 26, 254, "Lesson:254" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 253, 26, 253, "Lesson:253" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 252, 26, 252, "Lesson:252" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 251, 26, 251, "Lesson:251" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 250, 25, 250, "Lesson:250" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 249, 25, 249, "Lesson:249" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 248, 25, 248, "Lesson:248" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 247, 25, 247, "Lesson:247" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 245, 25, 245, "Lesson:245" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 151, 16, 151, "Lesson:151" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 150, 15, 150, "Lesson:150" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 149, 15, 149, "Lesson:149" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 53, 6, 53, "Lesson:53" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 52, 6, 52, "Lesson:52" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 51, 6, 51, "Lesson:51" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 50, 5, 50, "Lesson:50" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 49, 5, 49, "Lesson:49" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 48, 5, 48, "Lesson:48" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 54, 6, 54, "Lesson:54" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 47, 5, 47, "Lesson:47" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 45, 5, 45, "Lesson:45" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 44, 5, 44, "Lesson:44" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 43, 5, 43, "Lesson:43" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 42, 5, 42, "Lesson:42" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 41, 5, 41, "Lesson:41" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 40, 4, 40, "Lesson:40" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 46, 5, 46, "Lesson:46" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 55, 6, 55, "Lesson:55" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 56, 6, 56, "Lesson:56" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 57, 6, 57, "Lesson:57" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 72, 8, 72, "Lesson:72" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 71, 8, 71, "Lesson:71" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 70, 7, 70, "Lesson:70" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 69, 7, 69, "Lesson:69" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 68, 7, 68, "Lesson:68" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 67, 7, 67, "Lesson:67" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 66, 7, 66, "Lesson:66" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 65, 7, 65, "Lesson:65" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 64, 7, 64, "Lesson:64" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 63, 7, 63, "Lesson:63" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 62, 7, 62, "Lesson:62" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 61, 7, 61, "Lesson:61" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 60, 6, 60, "Lesson:60" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 59, 6, 59, "Lesson:59" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 58, 6, 58, "Lesson:58" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 39, 4, 39, "Lesson:39" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 38, 4, 38, "Lesson:38" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 37, 4, 37, "Lesson:37" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 36, 4, 36, "Lesson:36" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 16, 2, 16, "Lesson:16" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 15, 2, 15, "Lesson:15" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 14, 2, 14, "Lesson:14" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 13, 2, 13, "Lesson:13" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 12, 2, 12, "Lesson:12" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 11, 2, 11, "Lesson:11" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 10, 1, 10, "Lesson:10" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 9, 1, 9, "Lesson:9" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 8, 1, 8, "Lesson:8" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 7, 1, 7, "Lesson:7" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 6, 1, 6, "Lesson:6" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 5, 1, 5, "Lesson:5" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 4, 1, 4, "Lesson:4" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 3, 1, 3, "Lesson:3" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 2, 1, 2, "Lesson:2" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 17, 2, 17, "Lesson:17" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 73, 8, 73, "Lesson:73" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 18, 2, 18, "Lesson:18" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 20, 2, 20, "Lesson:20" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 35, 4, 35, "Lesson:35" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 34, 4, 34, "Lesson:34" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 33, 4, 33, "Lesson:33" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 32, 4, 32, "Lesson:32" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 31, 4, 31, "Lesson:31" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 30, 3, 30, "Lesson:30" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 29, 3, 29, "Lesson:29" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 28, 3, 28, "Lesson:28" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 27, 3, 27, "Lesson:27" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 26, 3, 26, "Lesson:26" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 25, 3, 25, "Lesson:25" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 24, 3, 24, "Lesson:24" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 23, 3, 23, "Lesson:23" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 22, 3, 22, "Lesson:22" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 21, 3, 21, "Lesson:21" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 19, 2, 19, "Lesson:19" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 74, 8, 74, "Lesson:74" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 75, 8, 75, "Lesson:75" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 76, 8, 76, "Lesson:76" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 129, 13, 129, "Lesson:129" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 128, 13, 128, "Lesson:128" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 127, 13, 127, "Lesson:127" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 126, 13, 126, "Lesson:126" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 125, 13, 125, "Lesson:125" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 124, 13, 124, "Lesson:124" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 123, 13, 123, "Lesson:123" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 122, 13, 122, "Lesson:122" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 121, 13, 121, "Lesson:121" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 120, 12, 120, "Lesson:120" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 119, 12, 119, "Lesson:119" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 118, 12, 118, "Lesson:118" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 117, 12, 117, "Lesson:117" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 116, 12, 116, "Lesson:116" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 115, 12, 115, "Lesson:115" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 130, 13, 130, "Lesson:130" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 114, 12, 114, "Lesson:114" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 131, 14, 131, "Lesson:131" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 133, 14, 133, "Lesson:133" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 148, 15, 148, "Lesson:148" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 147, 15, 147, "Lesson:147" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 146, 15, 146, "Lesson:146" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 145, 15, 145, "Lesson:145" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 144, 15, 144, "Lesson:144" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 143, 15, 143, "Lesson:143" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 142, 15, 142, "Lesson:142" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 141, 15, 141, "Lesson:141" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 140, 14, 140, "Lesson:140" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 139, 14, 139, "Lesson:139" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 138, 14, 138, "Lesson:138" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 137, 14, 137, "Lesson:137" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 136, 14, 136, "Lesson:136" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 135, 14, 135, "Lesson:135" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 134, 14, 134, "Lesson:134" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 132, 14, 132, "Lesson:132" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 299, 30, 299, "Lesson:299" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 113, 12, 113, "Lesson:113" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 111, 12, 111, "Lesson:111" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 91, 10, 91, "Lesson:91" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 90, 9, 90, "Lesson:90" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 89, 9, 89, "Lesson:89" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 88, 9, 88, "Lesson:88" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 87, 9, 87, "Lesson:87" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 86, 9, 86, "Lesson:86" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 85, 9, 85, "Lesson:85" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 84, 9, 84, "Lesson:84" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 83, 9, 83, "Lesson:83" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 82, 9, 82, "Lesson:82" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 81, 9, 81, "Lesson:81" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 80, 8, 80, "Lesson:80" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 79, 8, 79, "Lesson:79" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 78, 8, 78, "Lesson:78" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 77, 8, 77, "Lesson:77" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 92, 10, 92, "Lesson:92" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 112, 12, 112, "Lesson:112" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 93, 10, 93, "Lesson:93" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 95, 10, 95, "Lesson:95" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 110, 11, 110, "Lesson:110" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 109, 11, 109, "Lesson:109" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 108, 11, 108, "Lesson:108" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 107, 11, 107, "Lesson:107" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 106, 11, 106, "Lesson:106" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 105, 11, 105, "Lesson:105" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 104, 11, 104, "Lesson:104" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 103, 11, 103, "Lesson:103" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 102, 11, 102, "Lesson:102" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 101, 11, 101, "Lesson:101" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 100, 10, 100, "Lesson:100" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 99, 10, 99, "Lesson:99" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 98, 10, 98, "Lesson:98" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 97, 10, 97, "Lesson:97" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 96, 10, 96, "Lesson:96" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 94, 10, 94, "Lesson:94" });

            migrationBuilder.InsertData(
                table: "Lesson",
                columns: new[] { "Id", "ChapterId", "Index", "Name" },
                values: new object[] { 300, 30, 300, "Lesson:300" });

            migrationBuilder.CreateIndex(
                name: "IX_Chapter_CourseId",
                table: "Chapter",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_EnrolledCourses_CourseId",
                table: "EnrolledCourses",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_EnrolledCourses_UserId",
                table: "EnrolledCourses",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Lesson_ChapterId",
                table: "Lesson",
                column: "ChapterId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompletedLessons");

            migrationBuilder.DropTable(
                name: "Lesson");

            migrationBuilder.DropTable(
                name: "EnrolledCourses");

            migrationBuilder.DropTable(
                name: "Chapter");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Courses");
        }
    }
}
