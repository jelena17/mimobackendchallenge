﻿using Domain.Courses;
using Domain.Users;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Data
{
    public static class SeedExtensions
    {
        private static Course[] courses = DefaultCourseData();
        private static User user = DefaultUserData();

        public static void SeedUsers(this EntityTypeBuilder<User> builder)
            => builder.HasData(new { Id = user.Id });

        public static void SeedEnrollments(this OwnedNavigationBuilder<User, Enrollement> builder)
        {
            var data = user.Enrollements.Select(x => new
            {
                Id = 1,
                CourseId = x.Course.Id,
                UserId = user.Id
            });
            builder.HasData(data);
        }

        public static void SeedLessonsCompleted(this OwnedNavigationBuilder<Enrollement, CompletedLesson> builder)
        {
            var lessons = user.Enrollements.SelectMany(x => x.CompletedLessons);
            var data = lessons.Select(x => new
            {
                Id = x.Id,
                EnrollementId = 1,
            });
            builder.HasData(data);
        }

        public static void SeedInterval(this OwnedNavigationBuilder<CompletedLesson, DateInterval> builder)
        {
            var lessons = user.Enrollements.SelectMany(x => x.CompletedLessons);
            var data = lessons.Select(x => new
            {
                CompletedLessonEnrollementId = 1,
                CompletedLessonId = x.Id,
                Start = x.CompletitionInterval.Start,
                End = x.CompletitionInterval.End,
            });
            builder.HasData(data);
        }

        public static void SeedCourses(this EntityTypeBuilder<Course> builder)
        {
            var data = courses.Select((c, inx) => new
            {
                Id = c.Id,
                Name = c.Name,
            });
            builder.HasData(data);
        }

        public static void SeedChapters(this EntityTypeBuilder<Chapter> builder)
        {
            foreach (var course in courses)
            {
                var chapters = course.Chapters
                    .Select((c, inx) => new
                    {
                        Id = c.Id,
                        Name = c.Name,
                        CourseId = course.Id,
                        Index = c.Index,
                    });
                builder.HasData(chapters);
            }
        }

        public static void SeedLessons(this EntityTypeBuilder<Domain.Courses.Lesson> builder)
        {
            foreach (var chapter in courses.SelectMany(c => c.Chapters))
            {
                var lessons = chapter.Lessons
                    .Select((c) => new
                    {
                        Id = c.Id,
                        Name = c.Name,
                        Index = c.Index,
                        ChapterId = chapter.Id,
                    });
                builder.HasData(lessons);
            }
        }

        public static User DefaultUserData()
        {
            var completedLessons = Enumerable.Range(1, 50)
               .Select(inx => new CompletedLesson(inx, DateInterval.Create(DateTime.UtcNow.AddMinutes(-30), DateTime.UtcNow)))
               .ToList();

            var course = CreateCourse("C#", id: 1);

            var enrolledCourses = new Enrollement(
                course,
                completedLessons);

            return new User(1, new[] { enrolledCourses });
        }

        private static Course[] DefaultCourseData()
        {
            return new Course[] {
                  CreateCourse("C#",1),
                  CreateCourse("Swift",2),
                  CreateCourse("JavaScript",3)
            };
        }

        private static Course CreateCourse(string name, int id)
        => new Course(id, name, CreateChapters(id));

        private static IEnumerable<Chapter> CreateChapters(int courseid)
        {
            foreach (var chapterIndex in Enumerable.Range(((courseid - 1) * 10) + 1, 10))
            {
                var lessons = Enumerable.Range(((chapterIndex - 1) * 10) + 1, 10)
                     .Select(l => new Lesson(id: l, index: l, $"Lesson:{l}"));

                yield return new Chapter(
                      id: chapterIndex,
                      index: chapterIndex,
                      name: $"Chapter:{chapterIndex}",
                      lessons: lessons);
            }
        }
    }
}