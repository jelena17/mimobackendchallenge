﻿using Domain.Achievements;
using Domain.Users;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Queries
{
    public class UserAchievementQuery
        : IQuery<int, IEnumerable<AchivementResponse>>
    {
        private readonly IUserRepository _userRepository;
        private readonly IAchievementCalculationStrategy _achivementCalculationStrategy;

        public UserAchievementQuery(IUserRepository userRepository,
            IAchievementCalculationStrategy achivmentCalculationStrategy)
        {
            _userRepository = userRepository;
            _achivementCalculationStrategy = achivmentCalculationStrategy;
        }

        public async Task<IEnumerable<AchivementResponse>> Execute(int userId)
        {
            User user = await _userRepository.Get(userId);
            var achivements = _achivementCalculationStrategy.GetAchievements(user);

            return achivements.Select(achivement
                => new AchivementResponse
                {
                    CourseName = achivement.CourseName,
                    Objective = achivement.Objective,
                    IsCompleted = achivement.IsCompleted,
                    Progress = achivement.Progress
                });
        }
    }
}