﻿using Application.Contracts.Attributes;

namespace Application.Contracts
{
    public class LessonsDto
    {
        [NotDefault]
        public int Index { get; set; }

        [NotDefault]
        public string Name { get; set; }
    }
}