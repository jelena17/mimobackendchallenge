﻿using Application.Contracts.Attributes;
using System;

namespace Application
{
    public class CompleteLessonRequest
    {
        internal int UserId { get; set; }

        [NotDefault]
        public int LessonId { get; set; }

        [NotDefault]
        public DateTime LessonStarted { get; set; }

        [NotDefault]
        public DateTime LessonCompleted { get; set; }

        public void SetUser(int id) => UserId = id;
    }
}