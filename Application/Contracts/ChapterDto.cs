﻿using Application.Contracts.Attributes;

namespace Application.Contracts
{
    public class ChapterDto
    {
        [NotDefault]
        public int Index { get; set; }

        [NotDefault]
        public string Name { get; set; }

        [NotDefault]
        public LessonsDto[] Lessons { get; set; }
    }
}