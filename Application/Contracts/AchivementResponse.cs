﻿using Domain.Achievements;

namespace Application
{
    public class AchivementResponse
    {
        public string CourseName { get; set; }
        public Objective Objective { get; set; }
        public bool IsCompleted { get; set; }
        public int? Progress { get; set; }
    }
}