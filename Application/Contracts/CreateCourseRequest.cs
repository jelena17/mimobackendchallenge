﻿using Application.Contracts.Attributes;

namespace Application.Contracts
{
    public class CreateCourseRequest
    {
        [NotDefault]
        public string Name { get; set; }

        [NotDefault]
        public ChapterDto[] Chapters { get; set; }
    }
}