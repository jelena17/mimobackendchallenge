﻿using Domain.Courses;
using System.Threading.Tasks;

namespace Application
{
    public interface ICourseRepository
    {
        Task Create(Course course);

        Task<Course> Get(int id);
    }
}