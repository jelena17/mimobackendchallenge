﻿using Domain.Users;
using System.Threading.Tasks;

namespace Application
{
    public interface IUserRepository
    {
        Task<User> Get(int userId);

        Task Save(User user);
    }
}