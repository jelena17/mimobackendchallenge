﻿using Application.Contracts;
using Domain.Courses;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Commands
{
    public class CreateCourseCommand
        : ICommand<CreateCourseRequest>
    {
        private readonly ICourseRepository _repo;

        public CreateCourseCommand(ICourseRepository repo)
        {
            _repo = repo;
        }

        public Task Execute(CreateCourseRequest request)
        {
            Course newCourse = CreateCourse(request);
            return _repo.Create(newCourse);
        }

        private Course CreateCourse(CreateCourseRequest request)
        {
            var chapters = request.Chapters
                .Select(c => new Chapter(
                    index: c.Index,
                    name: c.Name,
                    lessons: c.Lessons.Select(l => new Lesson(l.Index, l.Name))));

            return new Course(request.Name, chapters);
        }
    }
}