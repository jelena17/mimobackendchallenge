﻿using Domain.Users;
using System.Threading.Tasks;

namespace Application.Commands
{
    public class UserCompletesLessonCommand
        : ICommand<CompleteLessonRequest>
    {
        private readonly IUserRepository _userRepository;

        public UserCompletesLessonCommand(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task Execute(CompleteLessonRequest request)
        {
            User user = await _userRepository.Get(request.UserId);

            var lesson = new CompletedLesson(
                request.LessonId,
                new DateInterval(request.LessonStarted, request.LessonCompleted));

            user.CompleteLesson(lesson);

            await _userRepository.Save(user);
        }
    }
}