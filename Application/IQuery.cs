﻿using System.Threading.Tasks;

namespace Application
{
    public interface IQuery<TRequest, TResult>
    {
        Task<TResult> Execute(TRequest request);
    }
}